# ⚙️ Rock Paper Scissors, SHOT!
I made this app as a material to learn makin' Firefox extension rather then a game for having fun. But if you like it, it still be my huge pleasure. If you interest in the source code. Here is it. You can have it for studying Firefox extension. In the end, this simple game is just like a `Hello, World!` app.

# Where do we start?
- Users can download extension [for Firefox at AMO](https://addons.mozilla.org/en-US/firefox/addon/dt-rps).
- For who interesting in source code, please go to `src` directory.
- The `res` dir is resource, which contains graphic assets and credit of them (like where did I get it or its licence).

# What's new?
In the `version 1.0`, players can have a very fundamental feature of the game, Player vs Computer. This version also support Firefox Android so we can kill time while riding buses. So here is what it got:
1. Play Rock Paper Scissors with Computer
2. Completely offline
3. Support Firefox Android
4. No ads (even if I wanna, I still have no idea how to do it - LOL)

# Feedback
I always love to improve my works, so feedbacks are very welcomed. You can open issues for feedback such as: requesting features, reporting bugs, code question, etc. However, there are still some limit that I might not be able to reply or resolve all the problems. If in that case, I hope you would understand it for me.