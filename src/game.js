var hMoves = document.querySelectorAll('.human > input[type=radio]');
var cMoves = document.querySelectorAll('.computer > input[type=radio]');
var board = document.querySelector('#result');
cMoves.forEach(c => c.disabled = true);


function rand(min, max) {
	min = parseInt(min);
	max = parseInt(max);
	let dice = Math.floor((Math.random() * max) + min);
	return dice ? dice : false;
}

function vs(p1, p2) {
	let left = p1 - p2;
	board.style.visibility = 'visible';
	switch (left) {
		case -2:
		case 1:
			board.innerHTML = 'You win!';
			break;

		case 2:
		case -1:
			board.innerHTML = 'You lose!';
			break;

		case 0:
			board.innerHTML = 'draw!';
	}
}

/**
 * Main func
 */
hMoves.forEach(h => {
	h.onclick = () => {
		p1 = h.value;
		let p2 = rand(1, 3);
		cMoves.forEach(c => { if (c.value == p2) c.checked = true })
		vs(p1, p2);
	}
})
